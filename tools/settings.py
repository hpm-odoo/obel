environment_vars = {
    "my-obel-server": {
        "production": {
            "path": "/path/to/odoo-bin",
            "db_name": "your-dbname",
            "conf_path": "/path/to/odoo.conf",
            "default_port": 8080,
            "base_url": "url/ip/domain to login odoo"
        },
        "staging": {
            "path": "/path/to/odoo-bin",
            "db_name": "your-dbname",
            "conf_path": "/path/to/odoo.conf",
            "default_port": 8080,
            "base_url": "url/ip/domain to login odoo"
        },
        "testing": {
            "path": "/path/to/odoo-bin",
            "db_name": "your-dbname",
            "conf_path": "/path/to/odoo.conf",
            "default_port": 8080,
            "base_url": "url/ip/domain to login odoo"
        }
    }
}
secret_key = 'my-secret-key'
entity = 'my-obel-server'