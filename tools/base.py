import re
import sys
import json
import logging
import requests
from time import sleep
from io import StringIO
from datetime import datetime, timedelta
from base64 import urlsafe_b64encode as encode
from hashlib import sha1
import codecs
import jwt
import os
from . import settings

entity = settings.entity
env_state = 'staging'
environment_var = settings.environment_vars.get(entity, {}).get(env_state)
if not environment_var:
    logging.warning('[setup_config] entity=%s or env_state=%s not found' % (entity, env_state))
    exit()
sys.path.append(environment_var.get('path'))

import odoo
from dis import get_instructions
from opcode import opmap, opname
from odoo.tools.safe_eval import _SAFE_OPCODES, test_expr, ustr
from odoo.exceptions import ValidationError

# OPCODES
_SAFE_OPCODES_EXTENTIONS = ['STORE_ATTR']
_OPCODES_WRAPPER = _SAFE_OPCODES_EXTENTIONS

# REGEX FOR FORBIDDEN COMMAND
not_allowed_patterns = (
    r"\bselect\b[ ][\w\*\,\_\s]+[ ]\bfrom\b",
    r"\bdelete\b[ ]\bfrom\b",
    r"\bupdate\b[ ][\w\_\s]+\bset\b",
    r"\binsert\b[ ]\binto\b"
)


class TokenNotValid(Exception):
    """not valid token exception"""


def assert_normal_expression(expr):
    """ assert the expression to determining it's normal expr or using env inside
    :params expr: command or python block code -> print("Hello World")
    :return boolean
    """
    expr = re.sub(r'\s+', ' ', expr)
    sub_str = re.search(r'env\[|\=\senv|\.cr|cr\.|browse\(\[|search\(\[', expr, re.I)
    if sub_str:
        return False
    return True


def assert_expression_allowance(expr):
    """ assert commands are allowed and not allowed
    :params expr: command or python block code -> env.cr.execute("select * from sale_order limit 1")
    :return boolean
    """
    expr = re.sub(r'\s+', ' ', expr)
    for pattern in not_allowed_patterns:
        sub_str = re.search(pattern, expr, re.I)
        if sub_str:
            raise SyntaxError('this expr=%s is not allowed caused contain = %s' % (expr, sub_str.group()))
    return None


def test_python_expr(expr, mode=eval):
    try:
        assert_expression_allowance(expr)
        test_expr(expr, _SAFE_OPCODES.union(set(opmap[x] for x in _OPCODES_WRAPPER if x in opmap)), mode=mode.__name__)
    except (SyntaxError, TypeError, ValueError) as err:
        if len(err.args) >= 2 and len(err.args[1]) >= 4:
            error = {
                'message': err.args[0],
                'filename': err.args[1][0],
                'lineno': err.args[1][1],
                'offset': err.args[1][2],
                'error_line': err.args[1][3],
            }
            msg = "%s : %s at line %d\n%s" % (
            type(err).__name__, error['message'], error['lineno'], error['error_line'])
        else:
            msg = ustr(err)
        return msg
    return False


def _get_opcodes(expr, mode=eval):
    """_get_opcodes(codeobj) -> [opcodes]
    Extract the actual opcodes as an iterator from a code object
    >>> c = compile("[1 + 2, (1,2)]", "", "eval")
    >>> list(_get_opcodes(c))
    [100, 100, 23, 100, 100, 102, 103, 83]
    """
    code_obj = compile(expr, "", mode=mode.__name__)
    return [i.opcode for i in get_instructions(code_obj)]


def choose_mode(expr):
    try:
        compile(expr, '<stdin>', 'eval')
    except SyntaxError:
        return exec
    return eval


def _execute(expr, mode, env=None):
    # Create the in-memory "file"
    tmp_out = StringIO()

    # Replace default stdout (terminal) with our stream
    sys.stdout = tmp_out

    code_obj = compile(expr, "", mode=mode.__name__)
    retval = mode(code_obj)
    if retval is not None:
        print(retval)

    # The original `sys.stdout` is kept in a special
    # dunder named `sys.__stdout__`. So you can restore
    # the original output stream to the terminal.
    sys.stdout = sys.__stdout__
    return tmp_out.getvalue().splitlines()


def execute(config, expr, UID=None):
    mode = choose_mode(expr)
    msg = test_python_expr(expr, mode)
    if msg:
        raise ValidationError(msg)

    if assert_normal_expression(expr=expr):
        output = _execute(expr, mode)
    else:
        output = execute_with_env(config, expr, mode, UID)
    return output


def execute_with_env(config, expr, mode=eval, UID=None):
    if not UID:
        UID = odoo.SUPERUSER_ID
    if not odoo.tools.config.options.get('load_language', False):
        odoo.tools.config.options['load_language'] = None
    odoo.tools.config.parse_config(['--config=%s' % config.get('conf_path')])
    output = []
    with odoo.api.Environment.manage():
        registry = odoo.registry(config.get('db_name'))
        with registry.cursor() as cr:
            ctx = odoo.api.Environment(cr, UID, {})['res.users'].context_get()
            env = odoo.api.Environment(cr, UID, ctx)
            exec_output = _execute(expr, mode, env=env)
            try:
                with cr.savepoint():
                    output.extend(exec_output)
            except Exception as e:
                output.extend(list(e.args))
            cr.rollback()
    return output


def authentication(username, password, base_url, db):
    if base_url.endswith('/'):
        base_url = base_url[:-1]
    base_url += '/web/session/authenticate'
    payload = {
        'id': 84932,
        'jsonrpc': '2.0',
        'method': 'call',
        'params': {
            'db': db,
            'login': username,
            'password': password
        }
    }
    headers = {'content-type': 'application/json', 'accept': 'application/json'}

    def _authentication(base_url, payload, headers):
        return requests.post(url=base_url, data=json.dumps(payload), headers=headers, cookies=None, timeout=None).json()

    session = None
    for i in range(3):
        response = _authentication(base_url=base_url, payload=payload, headers=headers)
        if 'result' in response:
            session = response['result']
            break
        elif 'error' in response:
            if response['error']['data']['message'] == 'Session expired':
                sleep(5)
                continue
            raise
    return session


def get_commit_token(session_token, secret_key):
    """

    @param session: session token
    @type session: string or None
    @return:
    @rtype:
    """
    payload = jwt.decode(session_token, secret_key, algorithms='HS256')
    payload['exp'] = datetime.utcnow() + timedelta(minutes=30)
    print(payload)
    commit_token = jwt.encode(payload, secret_key, algorithm='HS256')
    return commit_token


def get_my_session_token(session, secret_key):
    """

    @param session:
    @type session: dict or None
    @return: string session token
    @rtype: string
    """
    encoded_jwt = jwt.encode({
        'username': session.get('username'),
        'db': session.get('db')
    }, secret_key, algorithm='HS256')
    return encoded_jwt.decode('utf-8')


def commit_token_validation(token, secret_key, session):
    """
    commit token validation before execute block code
    @param token:
    @type token:
    @param secret_key:
    @type secret_key:
    @param session:
    @type session:
    @return:
    @rtype:
    """
    decoded_token = jwt.decode(token, secret_key, algorithm='HS256')
    username = decoded_token.get('username')
    db = decoded_token.get('db')
    if username and db:
        if username == session.get('username') and db == session.get('db'):
            return True
    return False


def verify_admin_user_password(username, password, base_url, db, secret_key):
    ssha = generate_ssha(password, secret_key)
    admin_token = jwt.encode({'username': username, 'password': ssha}, secret_key, algorithm='HS256')
    admin_list_file = os.path.join(os.path.dirname(__file__), '../admin/admin_list')
    with open(admin_list_file, 'r') as rf:
        lines = rf.readlines()
    res = False
    for line in lines:
        line = line.strip()
        if line == admin_token:
            res = True
            break

    if not res:
        username_list = os.path.join(os.path.dirname(__file__), '../admin/usernames')
        with open(username_list, 'r') as rf:
            lines = rf.readlines()
        for line in lines:
            line = line.strip()
            if line == username:
                result = authentication(username, password, base_url, db)
                if not result or not result.get('uid'):
                    return False
                with open(admin_list_file, 'a') as af:
                    af.write(admin_token.decode('utf-8') + '\n')
                res = True
    return res


def generate_ssha(password, secret_key):
    """

    @param password:
    @type password: string
    @param secret_key:
    @type secret_key: string
    @return:
    @rtype: string
    """
    new_var = password.encode().hex() + secret_key.encode().hex()
    n_hash = sha1(codecs.decode(new_var, 'hex'))
    return str(n_hash.digest())


if __name__ == '__main__':
    expression = """
so = env['sale.order'].search([], limit=1)
print(so.name)
"""
    # execute(config=environment_var, expr=expression)

    res = generate_ssha('hendra234', 'ular-melingkar-di-pagar-pak-jafar')
    print(res)
