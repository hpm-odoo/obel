from flask import Flask, request, Response, jsonify, render_template, session, flash, redirect, send_from_directory
from datetime import datetime, timedelta
from flask_autoindex import AutoIndex
from io import StringIO
import json
import sys
import traceback
import argparse
import os

from tools.base import execute, settings, authentication, get_my_session_token, get_commit_token, commit_token_validation, TokenNotValid, verify_admin_user_password

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'filesystem'
log_index = AutoIndex(app, os.path.join(os.path.curdir, 'log_dir'), add_url_rules=False)



@app.route('/log')
def autoindex():
    # path = os.path.join(os.path.dirname(__file__), 'log_dir')
    base_path = os.path.join(os.path.curdir, 'log_dir')
    path = request.args.get('path', '')
    if path.startswith('.'):
        filepath = base_path + path[1:]
        if os.path.isfile(filepath):
            filepath_list = filepath.split('/')
            directory = '/'.join(filepath_list[:-1])
            print(directory, filepath_list[-1])
            return send_from_directory(directory, filepath_list[-1])
    return log_index.render_autoindex(path=path)


def test_execute(expr):
    # Create the in-memory "file"
    tmp_out = StringIO()

    # Replace default stdout (terminal) with our stream
    sys.stdout = tmp_out

    code_obj = compile(expr, "", mode="exec")
    exec(code_obj)

    # The original `sys.stdout` is kept in a special
    # dunder named `sys.__stdout__`. So you can restore
    # the original output stream to the terminal.
    sys.stdout = sys.__stdout__
    return tmp_out.getvalue().splitlines()


@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=30)


@app.route('/')
@app.route('/index')
def root():
    if not session.get('session', {}).get('uid'):
        return render_template('login.html', company_name=settings.entity.upper())
    my_session_token = get_my_session_token(dict(session.get('session', {})), app.secret_key)
    print(my_session_token)
    return render_template('editor.html', my_session_token=my_session_token, secret_key=app.secret_key, display_name=session['session'].get('name'))


@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        result = authentication(username, password, base_url=app.env_config.get('base_url'), db=app.env_config.get('db_name'))
        if not result or not result.get('uid'):
            flash('Something went wrong !')
            return render_template('login.html', company_name=settings.entity.upper())
        session['session'] = result
        return redirect('/')
    return Response(json.dumps({'data': 'method not allowed'}), status=405)


@app.route('/logout')
def logout():
    session.clear()
    return redirect('/index')


@app.route('/commit/token', methods=['GET', 'POST'])
def generate_commit_token():
    if request.method == 'GET':
        return render_template('commit_token_form.html')
    elif request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        result = verify_admin_user_password(username, password, app.env_config.get('base_url'), app.env_config.get('db_name'), app.secret_key)
        if not result:
            return 'Access Denied !'
        session_token = request.form.get('session_token')
        return get_commit_token(session_token, app.secret_key)


@app.route('/run', methods=['POST'])
def run():
    if request.method == 'POST':
        secret_key = request.json.get('secret_key')
        expr = request.json.get('expr')
        commit_token = request.json.get('commit_token')
        if app.secret_key == secret_key:
            try:
                if commit_token:
                    is_valid_commit_token = commit_token_validation(commit_token, app.secret_key, session.get('session', {}))
                    if not is_valid_commit_token:
                        raise TokenNotValid('Your Token Not Valid')
                output = execute(config=app.env_config, expr=expr, UID=session['session']['uid'])

                # log the expression if there is commit
                if commit_token:
                    current_dir = os.path.dirname(__file__)
                    log_dir = os.path.join(current_dir, 'log_dir')
                    if not os.path.exists(log_dir):
                        os.mkdir(log_dir)
                    current_month_dir = os.path.join(log_dir, datetime.now().strftime('%Y-%m'))
                    if not os.path.exists(current_month_dir):
                        os.mkdir(current_month_dir)
                    filename_format = '%s#[username=%s][db=%s].txt' % (
                        datetime.now().strftime('%Y-%m-%d_%H:%M:%S'),
                        session.get('session', {}).get('username'),
                        session.get('session', {}).get('db')
                    )
                    filename = os.path.join(current_month_dir, filename_format)
                    with open(filename, 'w') as wf:
                        wf.write(expr)
                        wf.write('\n'*2)
                        wf.write(str(output))
                    if isinstance(output, list):
                        output.append('[#%$ Commit Success #%$]')

                # output = test_execute(expr)
            except KeyError as e:
                if e.args[0] == 'session':
                    return redirect('/logout')
                raise
            except Exception as e:
                print(traceback.format_exc())
                print('ERROR')
                return Response(json.dumps({'data': list(e.args)}), status=400, mimetype='application/json')
            return jsonify({'data': output})
        else:
            return Response(json.dumps({'data': 'invalid secret key'}), status=400, mimetype='application/json')
    return Response(status=405, mimetype='application/json')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--state')
    args = parser.parse_args()
    if not args.state:
        print('PLEASE INPUT STATE')
        exit()
    entity = settings.entity
    state = str(args.state).lower().strip()
    if not settings.environment_vars.get(entity):
        print('allowed entity : %s' % str(settings.environment_vars.keys()))
        exit()
    if not settings.environment_vars[entity].get(state):
        print('allowed state : %s' % str(settings.environment_vars[entity].keys()))
        exit()
    app.env = state
    app.entity = entity
    # app.env_config = {}
    app.env_config = settings.environment_vars[entity].get(app.env)
    app.token_pwd = 'rahasia1234098'
    app.secret_key = settings.secret_key
    app.run(debug=True,host='0.0.0.0', port=app.env_config.get('default_port') or 8080, threaded=True, use_reloader=False)